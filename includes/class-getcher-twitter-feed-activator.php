<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/includes
 * @author     Your Name <email@example.com>
 */
class Getcher_Twitter_Feed_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
