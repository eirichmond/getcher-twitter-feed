<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/includes
 * @author     Your Name <email@example.com>
 */
class Getcher_Twitter_Feed_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
