<?php
	
/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://www.squareonemd.co.uk
 * @since      1.0.0
 *
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Getcher_Twitter_Feed
 * @subpackage Getcher_Twitter_Feed/admin
 * @author     Your Name <email@example.com>
 */
class Getcher_Twitter_Feed_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $getcher_twitter_feed    The ID of this plugin.
	 */
	private $getcher_twitter_feed;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $getcher_twitter_feed       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $getcher_twitter_feed, $version ) {

		$this->getcher_twitter_feed = $getcher_twitter_feed;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Getcher_Twitter_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Getcher_Twitter_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->getcher_twitter_feed, plugin_dir_url( __FILE__ ) . 'css/getcher-twitter-feed-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Getcher_Twitter_Feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Getcher_Twitter_Feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->getcher_twitter_feed, plugin_dir_url( __FILE__ ) . 'js/getcher-twitter-feed-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Add options page
     */
    public function add_plugin_page()
    {

        // This page will sit under "Settings"
        add_options_page(
            'Settings Getcher', 
            'Getcher Twitter', 
            'manage_options', 
            'getcher-setting-admin', 
            array( $this, 'getcher_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function getcher_admin_page()
    {
        // Set class property
        $this->options = get_option( 'getcher_option_name' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Getcher Twitter Feed</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'getcher_option_group' );   
                do_settings_sections( 'getcher-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     * @TODO clean up comments
     * @TODO add a title option
     * @TODO add a css overide option,
     *   check priorty loading and css specificity 
     *   the theme might override naturally 
     */
    public function page_init()
    {        
        register_setting(
            'getcher_option_group', // Option group
            'getcher_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'API settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'getcher-setting-admin' // Page
        );  

        add_settings_field(
            'getcher_consumerKey', // ID
            'Consumer Key', // Title 
            array( $this, 'getcher_consumerKey_callback' ), // Callback
            'getcher-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'getcher_consumerSecret', // ID
            'Consumer Secret', // Title 
            array( $this, 'getcher_consumerSecret_callback' ), // Callback
            'getcher-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'getcher_accessToken', // ID
            'Access Token', // Title 
            array( $this, 'getcher_accessToken_callback' ), // Callback
            'getcher-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'getcher_accessSecret', // ID
            'Access Secret', // Title 
            array( $this, 'getcher_accessSecret_callback' ), // Callback
            'getcher-setting-admin', // Page
            'setting_section_id' // Section           
        );      

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['getcher_consumerKey'] ) )
            $new_input['getcher_consumerKey'] = strip_tags( $input['getcher_consumerKey'] );

        if( isset( $input['getcher_consumerSecret'] ) )
            $new_input['getcher_consumerSecret'] = strip_tags( $input['getcher_consumerSecret'] );

        if( isset( $input['getcher_accessToken'] ) )
            $new_input['getcher_accessToken'] = strip_tags( $input['getcher_accessToken'] );

        if( isset( $input['getcher_accessSecret'] ) )
            $new_input['getcher_accessSecret'] = strip_tags( $input['getcher_accessSecret'] );


        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter all Twitter App settings here, these can be obtained from Twitter:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function getcher_consumerKey_callback()
    {
        printf(
            '<input type="text" id="getcher_consumerKey" name="getcher_option_name[getcher_consumerKey]" value="%s" />',
            isset( $this->options['getcher_consumerKey'] ) ? esc_attr( $this->options['getcher_consumerKey']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function getcher_consumerSecret_callback()
    {
        printf(
            '<input type="text" id="getcher_consumerSecret" name="getcher_option_name[getcher_consumerSecret]" value="%s" />',
            isset( $this->options['getcher_consumerSecret'] ) ? esc_attr( $this->options['getcher_consumerSecret']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function getcher_accessToken_callback()
    {
        printf(
            '<input type="text" id="getcher_accessToken" name="getcher_option_name[getcher_accessToken]" value="%s" />',
            isset( $this->options['getcher_accessToken'] ) ? esc_attr( $this->options['getcher_accessToken']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function getcher_accessSecret_callback()
    {
        printf(
            '<input type="text" id="getcher_accessSecret" name="getcher_option_name[getcher_accessSecret]" value="%s" />',
            isset( $this->options['getcher_accessSecret'] ) ? esc_attr( $this->options['getcher_accessSecret']) : ''
        );
    }

    public function load_widget()
    {
        register_widget( 'Getcher_Twitter' );
    }


}

class Getcher_Twitter extends WP_Widget {
	
	private $twitter_title = "My Tweets";
	
	private $twitter_username = "elliottrichmond";
	
	private $twitter_postcount = "3";
	
	private $twitter_follow_text = "Follow Me On Twitter";
	
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
			'getcher_twitter_feed',
			'Twitter Feed Widget',
			array(
				'classname'		=>	'getcher-twitter-feed',
				'description'	=>	__('A widget that displays your latest tweets.', 'framework')
			)
		);

		$this->register_scripts_and_styles();
	}

	/**
	 * Registers and enqueues stylesheets for the administration panel and the
	 * public facing site.
	 */
	public function register_scripts_and_styles() {
		wp_enqueue_style( 'getcher-styles', plugin_dir_url( __FILE__ ) . 'css/getcher-twitter-feed-admin.css' );
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		
		extract( $args );
		/* Our variables from the widget settings. */
		$this->twitter_title = apply_filters('widget_title', $instance['title'] );
		$this->twitter_username = $instance['username'];
		$this->twitter_postcount = $instance['postcount'];
		$this->twitter_follow_text = $instance['tweettext'];
		$transName = 'list_tweets';
		$twitterData = get_transient($transName);
		
	    $cacheTime = 20;
	    
	    //if(false === ($twitterData = get_transient($transName) ) ){
		if(empty($twitterData) ){

	    	require_once plugin_dir_path( __FILE__ ) . 'partials/twitteroauth.php';

	    	$getcher_settings = get_option('getcher_option_name');
	    	
			$twitterConnection = new TwitterOAuth(
				$getcher_settings['getcher_consumerKey'], // Consumer Key
				$getcher_settings['getcher_consumerSecret'], // Consumer secret
				$getcher_settings['getcher_accessToken'], // Access token
				$getcher_settings['getcher_accessSecret'] // Access token secret
			);
								
			$twitterData = $twitterConnection->get(
				'statuses/user_timeline',
				array(
					'screen_name'     => $this->twitter_username,
					'count'           => $this->twitter_postcount,
					'exclude_replies' => false,
					'tweet_mode' => 'extended'
				)
			);
			if($twitterConnection->http_code != 200)
			{
				$twitterData = get_transient($transName);
			}
			
	        // Save our new transient.
	        set_transient($transName, $twitterData, 60 * $cacheTime);
	    }
	    
		/* Before widget (defined by themes). */
		echo $before_widget;
		?>
		<?php
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $this->twitter_title )
			echo $before_title . $this->twitter_title . $after_title;
		/* Display Latest Tweets */
		 ?>
		 	<div class="twscroll" style="height:400px;">
		 	
			<a href="https://twitter.com/<?php echo esc_html_e($this->twitter_username); ?>"
				class="twitter-follow-button"
				data-show-count="true"
				data-lang="en">Follow @<?php echo esc_html_e($this->twitter_username); ?></a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            <?php
            	if(!empty($twitterData) || !isset($twitterData['error'])){
	            	
            		$i=0;
					$hyperlinks = true;
					$encode_utf8 = true;
					$twitter_users = true;
					$update = true;
					echo '<ul class="twitter_update_list">';
					
		            foreach($twitterData as $item){
			            
			            //var_dump($item);
			            
		                    $msg = $item->full_text;
		                    $image = $item->entities->media[0]->media_url_https;

		                    $permalink = 'http://twitter.com/#!/'. $this->twitter_username .'/status/'. $item->id_str;
		                    if($encode_utf8) $msg = esc_html($msg);
                                    $msg = $this->encode_tweet($msg);
		                    $link = $permalink;
		                     echo '<li class="twitter-item">';
		                      if ($hyperlinks) {    $msg = $this->hyperlinks($msg); }
		                      if ($twitter_users)  { $msg = $this->twitter_users($msg); }
		                      echo $msg;
		                      if ($image){
			                      echo '<span class="twpic"><img src="'.esc_html($image).'"></span>';
		                      }
		                    if($update) {
		                      $time = strtotime($item->created_at);
		                      $tweetid = $item->id;
		                      if ( ( abs( time() - $time) ) < 86400 )
		                        $h_time = sprintf( __('<br> %s ago'), human_time_diff( $time ) );
		                      else
		                        $h_time = date(__('Y/m/d'), $time);
		                      echo sprintf( __('%s', 'twitter-for-wordpress'),' <span class="twitter-timestamp"><abbr title="' . date(__('Y/m/d H:i:s'), $time) . '"><a href="https://twitter.com/elliottrichmond/status/'.$tweetid.'" target="_blank">' . $h_time . '</a></abbr></span>' );
		                     }
		                    echo '</li>';
		                    
		                    $i++;
		                    if ( $i >= $this->twitter_postcount ) break;
		            }
					echo '</ul>
';
            	}
            ?>
            </div>

		<?php
		/* After widget (defined by themes). */
		echo $after_widget;
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		// Strip tags to remove HTML (important for text inputs)
		foreach($new_instance as $k => $v){
			$instance[$k] = strip_tags($v);
		}
		return $instance;
	}
	/**
	 * Create the form for the Widget admin
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array(
		'title' => $this->twitter_title,
		'username' => $this->twitter_username,
		'postcount' => $this->twitter_postcount,
		'tweettext' => $this->twitter_follow_text,
		);

		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		

		<!-- Widget Title: Text Input -->

			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		
		<!-- Username: Text Input -->

			<label for="<?php echo $this->get_field_id( 'username' ); ?>"><?php _e('Twitter Username e.g. elliottrichmond', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'username' ); ?>" name="<?php echo $this->get_field_name( 'username' ); ?>" value="<?php echo $instance['username']; ?>" />
		
		<!-- Postcount: Text Input -->

			<label for="<?php echo $this->get_field_id( 'postcount' ); ?>"><?php _e('Number of tweets (max 20)', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'postcount' ); ?>" name="<?php echo $this->get_field_name( 'postcount' ); ?>" value="<?php echo $instance['postcount']; ?>" />
		
		<!-- Tweettext: Text Input -->

			<label for="<?php echo $this->get_field_id( 'tweettext' ); ?>"><?php _e('Follow Text e.g. Follow me on Twitter', 'framework') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'tweettext' ); ?>" name="<?php echo $this->get_field_name( 'tweettext' ); ?>" value="<?php echo $instance['tweettext']; ?>" />
		
	<?php
	}
	/**
	 * Find links and create the hyperlinks
	 */
	private function hyperlinks($text) {
	    $text = preg_replace('/\b([a-zA-Z]+:\/\/[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<a href=\"$1\" class=\"twitter-link\">$1</a>", $text);
	    $text = preg_replace('/\b(?<!:\/\/)(www\.[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&%#+$*!]*)\b/i',"<a href=\"http://$1\" class=\"twitter-link\">$1</a>", $text);
	    // match name@address
	    $text = preg_replace("/\b([a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]*\@[a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]{2,6})\b/i","<a href=\"mailto://$1\" class=\"twitter-link\">$1</a>", $text);
	        //mach #trendingtopics. Props to Michael Voigt
	    $text = preg_replace('/([\.|\,|\:|\�|\�|\>|\{|\(]?)#{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/#search?q=$2\" class=\"twitter-link\">#$2</a>$3 ", $text);
	    return $text;
	}
	/**
	 * Find twitter usernames and link to them
	 */
	private function twitter_users($text) {
	       $text = preg_replace('/([\.|\,|\:|\�|\�|\>|\{|\(]?)@{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/$2\" class=\"twitter-user\">@$2</a>$3 ", $text);
	       return $text;
	}
        /**
         * Encode single quotes in your tweets
         */
        private function encode_tweet($text) {
                $text = mb_convert_encoding( $text, "HTML-ENTITIES", "UTF-8");
                return $text;
        }
 }
