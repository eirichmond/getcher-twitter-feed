<?php

/**
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.squareonemd.co.uk
 * @since             1.0.0
 * @package           Getcher_Twitter_Feed
 *
 * @wordpress-plugin
 * Plugin Name:       Getcher Twitter Feed
 * Plugin URI:        http://www.squareonemd.co.uk/
 * Description:       A simple twitter plugin to create a twitter feed with a widget, requires client app for twitter.
 * Version:           1.0.0
 * Author:            Elliott Richmond Square One
 * Author URI:        http://www.squareonemd.co.uk/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       getcher-twitter-feed
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-getcher-twitter-feed-activator.php
 */
function activate_getcher_twitter_feed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-getcher-twitter-feed-activator.php';
	Getcher_Twitter_Feed_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-getcher-twitter-feed-deactivator.php
 */
function deactivate_getcher_twitter_feed() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-getcher-twitter-feed-deactivator.php';
	Getcher_Twitter_Feed_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_getcher_twitter_feed' );
register_deactivation_hook( __FILE__, 'deactivate_getcher_twitter_feed' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-getcher-twitter-feed.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_getcher_twitter_feed() {

	$plugin = new Getcher_Twitter_Feed();
	$plugin->run();

}
run_getcher_twitter_feed();
